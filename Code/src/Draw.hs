module Draw(drawWorld) where

import Graphics.Gloss
import DrawingPositions
import Board

-- Given a world state, return a Picture which will render the world state.
-- Currently just draws a single blue circle as a placeholder.
--
-- This will need to extract the Board from the world state and draw it
-- as a grid plus pieces.

-- | Draws the various Pictures and lists of Pictures representing buttons (i.e. elements such as Play button, Main Menu, etc.) depending on the GameState.
drawWorld :: [Picture] -> ([Picture], [Picture]) -> Picture -> World -> Picture
drawWorld boardPics tilePics logopic w = case (gameState w) of 
                                        MainMenu -> Color white $ Pictures [ drawMainMenu w logopic, drawBoardSizeToggle w, drawTimeLimitToggle w, drawPlayerColorToggle w, drawAiToggle w, drawReversiToggle w, drawDifficultyButton w ]
                                        Paused   -> Color white $ drawPauseMenu w logopic
                                        Playing  -> Pictures  [
                                                    Translate (fst boardDrawOffset) (snd boardDrawOffset) $ Pictures [ case (size (board w)) of 
                                                                                                                            4 -> boardPics!!0
                                                                                                                            8 -> boardPics!!1
                                                                                                                            16 ->boardPics!!2, tiles ], 
                                                    Color white $ drawUIText (windowSize w) w,
                                                    drawLogo w logopic,
                                                    drawTimer w,
                                                    drawMainMenuButton w,
                                                    drawHintsIndicator w,
                                                    drawUndoButton w,
                                                    drawPauseButton w
                                                  ]
                                        otherwise -> Pictures  [
                                                     Translate (fst boardDrawOffset) (snd boardDrawOffset) $ Pictures [ case (size (board w)) of 
                                                                                                                            4 -> boardPics!!0
                                                                                                                            8 -> boardPics!!1
                                                                                                                            16 ->boardPics!!2, tiles ], 
                                                    Color white $ drawUIText (windowSize w) w,
                                                    drawTimer w,
                                                    drawWinMessage w
                                                  ]
                                    where tiles = case (showHints w) of 
                                                    True -> Translate ((-1)*((fromIntegral (size (board w)) / 2)*(tileSizePixels (board w)) - ((tileSizePixels (board w))/2))) ((fromIntegral (size (board w)) / 2)*(tileSizePixels (board w)) - ((tileSizePixels (board w))/2)) $ drawPieces (tileSizePixels (board w)) tilePics $ pieces (board w) ++ zip (getValidMoves (board w) (turn w)) (repeat Shadow)
                                                    False -> Translate ((-1)*((fromIntegral (size (board w)) / 2)*(tileSizePixels (board w)) - ((tileSizePixels (board w))/2))) ((fromIntegral (size (board w)) / 2)*(tileSizePixels (board w)) - ((tileSizePixels (board w))/2)) $ drawPieces (tileSizePixels (board w)) tilePics $ pieces (board w)

-- | Returns the picture associated with the according Piece (black, white, or shadow) at each position using 'drawPiece'
drawPieces :: Float -> ([Picture],[Picture]) -> [(Position, Col)] -> Picture
drawPieces _ _ [] = Blank
drawPieces tileSize tilePics (((x,y), col):rest) = Pictures [drawPiece tileSize tilePics x y col, drawPieces tileSize tilePics rest]

-- | Returns the picture for a black, white, or shadow piece. 
drawPiece :: Float -> ([Picture],[Picture]) -> Int -> Int -> Col -> Picture
drawPiece tileSize (_, whitePics) x y White  = Color white $ Translate (tileSize * fromIntegral x) ((-tileSize) * fromIntegral y) $ if tileSize == 40 then whitePics!!1 else whitePics!!0
drawPiece tileSize (blackPics, _) x y Black  = Color black $ Translate (tileSize * fromIntegral x) ((-tileSize) * fromIntegral y) $ if tileSize == 40 then blackPics!!1 else blackPics!!0
drawPiece tileSize _ x y Shadow = Color (makeColor8 40 40 40 120)  $ translate (tileSize * fromIntegral x) ((-tileSize) * fromIntegral y) $ circleSolid (tileSize * 0.37)

-- | Defines the colour of the background. 
darkGreen :: Color
darkGreen = dark $ dark $ dark green

-- | Returns the picture of the player labels as well as the scores. 
drawUIText :: (Int, Int) -> World -> Picture
drawUIText windowSize w = Pictures [ drawPlayerLabel windowSize $ turn w, drawScores windowSize (board w)]

-- | Returns the picture of the title during game play translated according to the size of the board.
drawTitle :: World -> Picture
drawTitle w =   if (size (board w)) == 16 && gameState w /= MainMenu then
                    Translate (fromIntegral (fst $ windowSize w) / (-2) + 30) (fromIntegral (snd $ windowSize w) / 2 - 100) $ Scale 0.4 0.4 $ (Text "OTHELLO")
                else
                    Translate (-110) (fromIntegral (snd $ windowSize w) / (2.5)) $ Scale 0.4 0.4 $ (Text "OTHELLO")

-- | Returns the picture (bitmap image named 'logopic') of the Othello logo.
drawLogo :: World -> Picture -> Picture
drawLogo w logopic = if (size (board w)) == 16 && gameState w /= MainMenu then
                        Translate (fromIntegral (fst $ windowSize w) / (-2) + 120) ((fromIntegral $ snd (windowSize w)) / 2 - 70) logopic
                     else
                        Translate 0 ((fromIntegral $ snd (windowSize w)) / 2 - 70) logopic

-- | Returns the Black player's turn label translated on the left side of screen and the White player's label translated on the right side of screen.
drawPlayerLabel :: (Int, Int) -> Col -> Picture
drawPlayerLabel (xSize,ySize) Black = Translate (fromIntegral xSize / (-2) + 30) 0 $ Scale 0.25 0.25 $ Text "Player 1 (Black)"
drawPlayerLabel (xSize,ySize) White = Translate (fromIntegral xSize / 3.6) 0 $ Scale 0.25 0.25 $ Text "Player 2 (White)"


-- | Returns the picture (which is really text) stating the specifics of the won game - Black Wins, White Wins, or Tie.
drawWinMessage :: World -> Picture
drawWinMessage w = Translate ((fromIntegral $ fst (windowSize w)) / 3.7) ((fromIntegral $ snd (windowSize w)) / 6) $ Scale 0.4 0.4 $ 
                            case (winner w) of 
                                Black -> Text "Black Wins"
                                White -> Text "White Wins"
                                otherwise -> Text "Tie"
                        
-- | Returns the picture of the scores for each player.
drawScores :: (Int, Int) -> Board -> Picture
drawScores (xSize, ySize) b = Translate (fromIntegral xSize / (-2) + 30) (fromIntegral ySize / 2 - 200) $ Scale 0.18 0.18 $ Text $ "BLACK: " ++ show (length $ filter (\ x -> x == Black) (map snd (pieces b))) ++ "\n WHITE: " ++ show (length $ filter (\ x -> x == White) (map snd (pieces b)))

-- | Returns the picture the Pause screen using 'drawTitle' for 'Othello' and 'drawPauseString' and 'drawPausedMessage' as helpers.
drawPauseMenu :: World -> Picture -> Picture
drawPauseMenu w logoPic = Pictures [ drawLogo w logoPic, drawPlayButton w ]

-- | Draws the timer translated based on windowSize. 
drawTimer :: World -> Picture
drawTimer w = if (timeLimit w) == 0 then
                Blank
              else
                Scale 0.5 0.5 $ Translate ((fromIntegral $ fst (windowSize w)) / 1.8) ((fromIntegral $ snd (windowSize w)) / 1.8) $ Text $ show $ (round $ timeLimit w) - (round $ timePassed w)

-- | Takes the World and the 'logopic' to return the picture of the Main Menu.
drawMainMenu :: World -> Picture -> Picture
drawMainMenu w logopic = Pictures [ Translate 0 ((fromIntegral $ snd (windowSize w)) / 2 - 70) logopic, drawPlayButton w]

-- | Returns a picture of the 'Play' button. 
drawPlayButton :: World -> Picture
drawPlayButton w = Translate (fst $ fst $ playButtonPos w) (snd $ fst $ playButtonPos w) $ Scale 0.5 0.5 $ Text "PLAY"

-- | Returns a picture of the 'Size' button, which changes the board size when the left click Event occurs. 
drawBoardSizeToggle :: World -> Picture
drawBoardSizeToggle w = Translate (fst $ fst $ boardSizeTogglePos w) (snd $ fst $ boardSizeTogglePos w) $ Scale 0.2 0.2 $ Text $ "Change board size: " ++ show (size (board w))

-- | Returns a picture of the 'Time Limit' button, which changes the time limit when the left click Event occurs. 
drawTimeLimitToggle :: World -> Picture
drawTimeLimitToggle w = Translate (fst $ fst $ timeLimitTogglePos w) (snd $ fst $ timeLimitTogglePos w) $ Scale 0.2 0.2 $ Text $ "Change time limit: " ++ show (timeLimit w)

-- | Returns a picture of the 'Player Color' button, which changes the player's colour when the left click Event occurs. 
drawPlayerColorToggle :: World -> Picture
drawPlayerColorToggle w = Translate (fst $ fst $ playerColorTogglePos w) (snd $ fst $ playerColorTogglePos w) $ Scale 0.2 0.2 $ Text $ "Player Color: " ++ show (playerColor w)

-- | Returns a picture of the 'AI' button, which changes the presence of the AI when the left click Event occurs. 
drawAiToggle :: World -> Picture
drawAiToggle w = Translate (fst $ fst $ aiTogglePos w) (snd $ fst $ aiTogglePos w) $ Scale 0.2 0.2 $ Text $ "AI: " ++ show (aiPlayer w)

-- | Returns a picture of the 'Reversi Mode' button, which changes to Reversi mode when the left click Event occurs. 
drawReversiToggle :: World -> Picture
drawReversiToggle w = Translate (fst $ fst $ reversiTogglePos w) (snd $ fst $ reversiTogglePos w) $ Scale 0.2 0.2 $ Text $ "Reversi Mode: " ++ show (reversiMode (board w))

-- | Returns a picture of the 'Difficulty' button, which changes the difficulty when the left click Event occurs. 
drawDifficultyButton :: World -> Picture
drawDifficultyButton w = Translate (fst $ fst $ aiDifficultyTogglePos w) (snd $ fst $ aiDifficultyTogglePos w) $ Scale 0.2 0.2 $ Text $ "Difficulty: " ++ show (difficulty w)

-- | Returns a picture of the 'Menu' button during game play.
drawMainMenuButton :: World -> Picture
drawMainMenuButton w = Translate (fst $ fst $ mainMenuButtonPos w) (snd $ fst $ mainMenuButtonPos w) $ Scale 0.3 0.3 $ Text "MENU"

-- | Returns a picture of the 'Hints' indicator, showing whether or not hints are 'ON' or 'OFF'.
drawHintsIndicator :: World -> Picture
drawHintsIndicator w = Translate (fst $ fst $ hintsIndicatorPos w) (snd $ fst $ hintsIndicatorPos w) $ Scale 0.2 0.2 $ Text $ "HINTS: " ++ hints
                        where hints = if showHints w == True then
                                        "ON"
                                      else
                                        "OFF"

-- | Returns a picture of the 'UNDO MOVE' button that is present during game play.
drawUndoButton :: World -> Picture
drawUndoButton w = Translate (fst $ fst $ undoButtonPos w) (snd $ fst $ undoButtonPos w) $ Scale 0.2 0.2 $ Text "UNDO MOVE"

drawPauseButton :: World -> Picture
drawPauseButton w = Translate (fst $ fst $ pauseButtonPos w) (snd $ fst $ pauseButtonPos w) $ Scale 0.2 0.2 $ Text "PAUSE"

-- Initial drawing code for the board and tiles, using gloss images.

--drawBoard :: Int -> Float -> Float -> Picture
--drawBoard 64 _ _ = Blank
--drawBoard pos xOffSet yOffset = case (pos `mod` 8 == 0) of 
--                                    True  -> Pictures [drawTile 0 (yOffset-tileSize), drawBoard (pos+1) 0 (yOffset-tileSize)] --Next row
--                                    False -> Pictures [drawTile (xOffSet+tileSize) yOffset, drawBoard (pos+1) (xOffSet+tileSize) yOffset] --Continue row
--drawBoard size = Pictures [ translate tileSize y (Pictures tile) | x<-xs, y<-ys]

--drawTile :: Float -> Float -> Picture
--drawTile x y = translate x y (Pictures tile)
--drawTile =  [(Pictures tile), translate 40 0 (Pictures tile), translate 80 0 (Pictures tile), translate 120 0 (Pictures tile), translate 0 40 (Pictures tile), translate 40 40 (Pictures tile), translate 0 (-40) (Pictures tile), translate (-40) 0 (Pictures tile)]

--tile :: Picture
--tile = Pictures [Polygon [(0,0), (0,tileSize), (tileSize,tileSize), (tileSize,0)], border, translate tileSize 0 border, rotate 90 border,  translate 0 tileSize (rotate 90 border)]

--border :: Picture
--border = Color black $ Line [(0,0), (0,tileSize)]