module Input(handleInput) where

import Graphics.Gloss.Interface.Pure.Game
import Graphics.Gloss
import Graphics.Gloss.Data.Point
import Graphics.Gloss.Data.Extent
import Board
import AI

import DrawingPositions

import Debug.Trace


-- Update the world state given an input event. Some sample input events
-- are given; when they happen, there is a trace printed on the console
--
-- trace :: String -> a -> a
-- 'trace' returns its second argument while printing its first argument
-- to stderr, which can be a very useful way of debugging!


-- | Takes an event, most of our logic is pattern matching on left mouse button clicks, but the pause menu is optionally triggered by a key press, P
--   The current World and uses the (x,y) position (in conjunction with the current gameState) to perform the neccessary actions to the game,
--   depending on where the event takes place. That is the method is the main connection between the actions performed within the UI and the actions performed within the program. 
handleInput :: Event -> World -> World
handleInput (EventMotion (x, y)) w 
    = w

handleInput (EventKey (MouseButton LeftButton) Up m (x, y)) w
    =   case (gameState w) of

            -- From the MainMenu gameState, clicking on the various buttons starts game play, includes AI, toggles through game board sizes, time limit, etc.
            MainMenu -> if buttonClicked (x,y) (playButtonPos w) then
                            w { gameState = Playing}
                        else if buttonClicked (x,y) (boardSizeTogglePos w) then
                            w { board = initBoard (toggleSize (size (board w))) (reversiMode (board w)) tileSize }
                        else if buttonClicked (x,y) (timeLimitTogglePos w) then
                            w { timeLimit = cycleTimeLimit (timeLimit w) }
                        else if buttonClicked (x,y) (playerColorTogglePos w) then
                            w { playerColor = other (playerColor w) }
                        else if buttonClicked (x,y) (aiTogglePos w) then
                            w { aiPlayer = not (aiPlayer w) }
                        else if buttonClicked (x,y) (reversiTogglePos w) then
                            w { board = initBoard (size (board w)) (not $ reversiMode (board w)) (tileSizePixels (board w)) }
                        else if buttonClicked (x,y) (aiDifficultyTogglePos w) then
                            w { difficulty = cycleDifficulty (difficulty w) }
                        else
                            w
                        where tileSize =  case (toggleSize (size (board w))) of -- Toggle size because we want the size after the change enacted by the button press.
                                                    4  -> 60
                                                    8  -> 60
                                                    16 -> 40


            -- Doesn't allow clicking and any other events to adjust the game whilst paused
            Paused  ->  if buttonClicked (x,y) (playButtonPos w) then
                            w { gameState = Playing } 
                        else 
                            trace ("the game is paused") w

            -- When in a 'Playing' gameState, the player can place the pieces freely in reversiMode, play a piece on the board, undo a move, or toggle hints
            Playing ->  if (turn w) == (playerColor w) || not (aiPlayer w) then
                            if buttonClicked (x,y) (mainMenuButtonPos w) then
                                initWorld (size (board w)) (playerColor w) (reversiMode (board w)) (timeLimit w) (windowSize w) (tileSizePixels $ board w) MainMenu (difficulty w) 
                            else if buttonClicked (x,y) (pauseButtonPos w) then
                                w { gameState = togglePause (gameState w) }
                            else if withinBoardBoundaries (board w) x y then 
                                trace ("Left button pressed at: " ++ show (xBoardOffset (board w) x, yBoardOffset (board w) y) ++ " relative to board.\n At POSITION: (" ++ show (coordX (board w) x) ++ "," ++ show (coordY (board w) y) ++ ")\nPieces: " ++ show (board w) ++ "\nPiece count: " ++ show (checkScore (board w)) ++ "\nPrev Boards: " ++ show (length (prevBoards w)) ++ "\nAI Corners Taken: " ++ show (aiCornersTaken (board w)) ++ "\nPlayer corners taken: " ++ show (playerCornersTaken (board w)) ++ "\nAI Adjacent Corners Taken: " ++ show (aiAdjacentCornersTaken (board w))) updateWorldWithBoard w ((coordX (board w) x), (coordY (board w) y))
                            else if buttonClicked (x,y) (undoButtonPos w) then
                                trace ("Left button pressed 'UNDO MOVE'. The previous boards: " ++ show (length (prevBoards w)) ) getBoardFromPrevious w
                            else 
                                trace ("Click was out of bounds of the board: " ++ show (xBoardOffset (board w) x, yBoardOffset (board w) y, ((-1)*(fromIntegral (size (board w)))*(tileSizePixels (board w))))) w { showHints = not $ showHints w }
                        else
                            w
                        where tileSize =  case (toggleSize (size (board w))) of -- Toggle size because we want the size after the change enacted by the button press.
                                                4  -> 60
                                                8  -> 60
                                                16 -> 40


            otherwise -> if withinBoardBoundaries (board w) x y then -- If the game is finished, click in board to reset, could be changed to a "New Game" button fairly easily.
                            initWorld (size (board w)) (playerColor w) (reversiMode (board w)) (timeLimit w) (windowSize w) (tileSizePixels (board w)) Playing (difficulty w)
                        else
                            w
handleInput e w = w

{- Hint: when the 'World' is in a state where it is the human player's
 turn to move, a mouse press event should calculate which board position
 a click refers to, and update the board accordingly.

 At first, it is reasonable to assume that both players are human players.
-}

-- | Checks that the point (x,y) is within the boundaries of board b
withinBoardBoundaries :: Board -> Float -> Float -> Bool
withinBoardBoundaries b x y = pointInBox (xBoardOffset b x, yBoardOffset b y) (0,0) ((fromIntegral (size b))*(tileSizePixels b) , ((-1)* fromIntegral (size b))*(tileSizePixels b))

-- | Checks if the play button has been clicked
playButtonClicked :: Point -> (Int, Int) -> Bool
playButtonClicked (x,y) (xSize, ySize) = pointInExtent (makeExtent 100 10 100 (-80)) (x,y)

-- | Detects if point is inside the box described by (Point, Point).
buttonClicked :: Point -> (Point, Point) -> Bool
buttonClicked (x,y) ((x2,y2),(x3,y3))= pointInBox (x,y) (x2,y2) (x3,y3)

-- | Used for toggling through the different board size options
toggleSize :: Int -> Int
toggleSize 4 = 8
toggleSize 8 = 16
toggleSize 16 = 4

-- | Toggles the gamestate between paused and playing.
togglePause :: GameState -> GameState
togglePause state = case state of
                        Paused    -> Playing
                        Playing   -> Paused
                        otherwise -> state

-- | Used for toggling through the different time limit options
cycleTimeLimit :: Float -> Float
cycleTimeLimit 0  = 5
cycleTimeLimit 5  = 10
cycleTimeLimit 10 = 20
cycleTimeLimit 20 = 30
cycleTimeLimit 30 = 60
cycleTimeLimit 60 = 0
cycleTimeLimit _  = 0

-- | Used for toggling through the different difficulty levels
cycleDifficulty :: Difficulty -> Difficulty
cycleDifficulty Easy    = Medium
cycleDifficulty Medium  = Hard
cycleDifficulty Hard    = Easy

-- | Originally this function was used to adjust the UI according to the size of the board (4,8,16), however we ended up adjusting the board itself by making the pieces smaller
getWindowSize :: Int -> (Int, Int)
getWindowSize 4 = (1280, 720)
getWindowSize 8 = (1280, 720)
getWindowSize 16 = (1280, 720)

-- | Adjusts the board such that the Gloss x-coordinate is in line with standard computing coordinate convention (i.e. 0,0 in upper left corner)
xBoardOffset :: Board -> Float -> Float
xBoardOffset b x = x - fst boardDrawOffset + (((fromIntegral $ size b)/2)*(tileSizePixels b)) 

-- | Adjusts the board such that the Gloss y-coordinate is in line with standard computing coordinate convention (i.e. 0,0 in upper left corner)
yBoardOffset :: Board -> Float -> Float
yBoardOffset b y = y - snd boardDrawOffset - (((fromIntegral $ size b)/2)*(tileSizePixels b))

-- | Converts GUI screen x-coordinate to board coordinate
coordX :: Board -> Float -> Int
coordX b x = floor $ (xBoardOffset b x) / tileSizePixels b

-- | Converts GUI screen x-coordinate to board coordinate
coordY :: Board -> Float -> Int 
coordY b y = floor $ abs $ (yBoardOffset b y) / tileSizePixels b
