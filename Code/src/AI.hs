module AI where

import Board
import Data.List
import Data.Ord

import Debug.Trace

data GameTree = GameTree { game_board :: Board,
                           game_turn :: Col,
                           next_moves :: [(Position, GameTree)] }

-- Given a function to generate plausible moves (i.e. board positions)
-- for a player (Col) on a particular board, generate a (potentially)
-- infinite game tree.
--
-- (It's not actually infinite since the board is finite, but it's sufficiently
-- big that you might as well consider it infinite!)
--
-- An important part of the AI is the 'gen' function you pass in here.
-- Rather than generating every possible move (which would result in an
-- unmanageably large game tree!) it could, for example, generate moves
-- according to various simpler strategies.

-- | Generates tree of moves, used for min-max AI implementation.
buildTree :: (Board -> Col -> [Position]) -- ^ Move generator
             -> Board -- ^ board state
             -> Col -- ^ player to play next
             -> GameTree
buildTree gen b c = let moves = gen b c in -- generated moves
                        GameTree b c (mkNextStates moves)
  where
    mkNextStates :: [Position] -> [(Position, GameTree)]
    mkNextStates [] = []
    mkNextStates (pos : xs)
        = case makeMove b c pos of -- try making the suggested move
               Nothing -> mkNextStates xs -- not successful, no new state
               Just b' -> (pos, buildTree gen b' (other c)) : mkNextStates xs
                             -- successful, make move and build tree from 
                             -- here for opposite player

-- Get the best next move from a (possibly infinite) game tree. This should
-- traverse the game tree up to a certain depth, and pick the move which
-- leads to the position with the best score for the player whose turn it
-- is at the top of the game tree.

-- | Selects the best moves from the GameTree. 
getBestMove ::  Difficulty 
                -> Col  -- ^ Player color, need to know which color to maximise/minimise for.
                -> Int -- ^ Maximum search depth
                -> GameTree -- ^ Initial game tree
                -> Position
getBestMove diff enemyColor depth tree = trace (show $ zip (map (evaluateMoves diff enemyColor depth) (map snd (next_moves tree))) (map fst (next_moves tree))) snd $ maximumBy (comparing fst) $ zip (map (evaluateMoves diff enemyColor depth) (map snd (next_moves tree))) (map fst (next_moves tree)) -- evaluate each of the next trees.

-- | Applies the evaluation functions to the subsequent difficulty levels and returns the adjusted scores based on the state of the board. 
evaluateMoves :: Difficulty -> Col -> Int -> GameTree -> Int
evaluateMoves diff _ 0 tree = evaluateFunc ((game_board tree), (game_turn tree))
                              where evaluateFunc = case diff of
                                                      Easy    -> easyEvaluate
                                                      Medium  -> harderEvaluate
                                                      Hard    -> harderEvaluate
evaluateMoves diff humanCol depth tree = if game_turn tree == humanCol then
                                        let scoreList = map (evaluateMoves diff humanCol (depth-1)) (map snd (next_moves tree)) in
                                                                if scoreList == [] then
                                                                    100 -- Forcing the other player to pass.
                                                                else
                                                                    (minimum scoreList) - depth
                                    else 
                                        let scoreList =   map (evaluateMoves diff humanCol (depth-1)) (map snd (next_moves tree)) in
                                                            if scoreList == [] then
                                                                100 -- Forcing the other player to pass.
                                                            else
                                                            (maximum scoreList) - depth 

instance Show World where
  show w = show (board w) ++ " " ++ show (turn w) ++ " " ++ show (playerColor w)

-- Update the world state after some time has passed, taking into account time limits.
updateWorld :: Float -- ^ time since last update (you can ignore this)
            -> World -- ^ current world state
            -> World
updateWorld t w = case gameState w of
                    Finished -> w { winner = determineWinner (board w) }
                    FinishedTimeLimit -> w
                    Playing ->  let validMoves = getValidMoves (board w) (turn w) in 
                                if (timePassed w) > aiThinkingTime && (aiPlayer w) && (turn w) /= (playerColor w) then -- Is AI player and "thinking" time has passed.
                                    case length validMoves == 0 of
                                        True -> w { board = (board w) { passes = (passes (board w)) + 1 } , turn = other (turn w), timePassed = 0, gameState = state }
                                        False -> updateWorldWithBoard w { timePassed = 0, gameState = state } $ getBestMove (difficulty w) (playerColor w) depth (buildTree getValidMoves (board w) (turn w))
                                else
                                    if (timePassed w) > (timeLimit w) && (timeLimit w) /= 0 then
                                        w { winner = other (turn w), gameState = FinishedTimeLimit }
                                    else
                                        case length validMoves == 0 of
                                            True -> w { board = (board w) { passes = (passes (board w)) + 1 } , turn = other (turn w), timePassed = (timePassed w) + 0.1, gameState = state }
                                            False -> w { board = (board w) { passes = 0 }, timePassed = (timePassed w) + 0.1, gameState = state }
                                where state = case gameOver (board w) of
                                                True  -> Finished
                                                False -> Playing
                                      depth = case difficulty w of --depth of the GameTree
                                                Easy    -> 2
                                                Medium  -> 0
                                                Hard    -> 2
                    otherwise -> w

-- | Defines the delay between the player's move and the AI's thinking time/move.
aiThinkingTime :: Float
aiThinkingTime = 0.3

{- Hint: 'updateWorld' is where the AI gets called. If the world state
 indicates that it is a computer player's turn, updateWorld should use
 'getBestMove' to find where the computer player should play, and update
 the board in the world state with that move.

 At first, it is reasonable for this to be a random valid move!

 If both players are human players, the simple version above will suffice,
 since it does nothing.

 In a complete implementation, 'updateWorld' should also check if either 
 player has won and display a message if so.
-}
