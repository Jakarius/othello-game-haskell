module DrawingPositions where

import Board
import Graphics.Gloss.Data.Point

{- 	
	The first Point is used for Translating the Picture for the object in the drawing code.
	The second Point is used to define a box in which clicks can be detected relating to the object.
-}
-- | Returns the point used for translating the Menu Picture and the point used for building the box for button use. 
mainMenuButtonPos :: World -> (Point, Point)
mainMenuButtonPos w = ((pos1, (fst $ pos1 + 100, snd $ pos1 + 50)))
                        where pos1 = (fromIntegral (fst (windowSize w)) / (-2) + 100, fromIntegral (snd (windowSize w)) / (-2) + 200)

-- | Returns the point used for translating the Hints indicator Picture.
hintsIndicatorPos :: World -> (Point, Point)
hintsIndicatorPos w = ((pos1, (fst $ pos1 + 100, snd $ pos1 + 50)))
                        where pos1 = (fromIntegral (fst (windowSize w)) / 2 - 200, fromIntegral (snd (windowSize w)) / (-2) + 40)

-- | Returns the point used for translating the UNDO MOVE Picture and the point used for building the box for button use. 
undoButtonPos :: World -> (Point, Point)
undoButtonPos w = ((pos1, (fst $ pos1 + 150, snd $ pos1 + 70)))
                        where pos1 = (fromIntegral (fst (windowSize w)) / 2 - 200, fromIntegral (snd (windowSize w)) / (-2) + 100)

-- | Returns the point used for translating the Play Picture and the point used for building the box for button use. 
playButtonPos :: World -> (Point, Point)
playButtonPos w = ((pos1, (fst $ pos1 + 130, snd $ pos1 + 80)))
                        where pos1 = ((-75),0)

-- | Returns the point used for translating the Size Picture within the Menu gamestate and the point used for building the box for handling clicks. 
boardSizeTogglePos :: World -> (Point, Point)
boardSizeTogglePos w = ((pos1, (fst $ pos1 + 290, snd $ pos1 + 50)))
                        where pos1 = (fromIntegral (fst (windowSize w)) / (-2) + 80, fromIntegral (snd (windowSize w)) / (-2) + 100)

-- | Returns the point used for translating the Time Limit Picture within the Menu gamestate and the point used for building the box for handling clicks. 
timeLimitTogglePos :: World -> (Point, Point)
timeLimitTogglePos w = ((pos1, (fst $ pos1 + 290, snd $ pos1 + 30)))
                        where pos1 = (fromIntegral (fst (windowSize w)) / (-2) + 430, fromIntegral (snd (windowSize w)) / (-2) + 50)

-- | Returns the point used for translating the Player Color Picture within the Menu gamestate and the point used for building the box for handling clicks. 
playerColorTogglePos :: World -> (Point, Point)
playerColorTogglePos w = ((pos1, (fst $ pos1 + 200, snd $ pos1 + 30)))
                        where pos1 = (fromIntegral (fst (windowSize w)) / (-2) + 780, fromIntegral (snd (windowSize w)) / (-2) + 50)

-- | Returns the point used for translating the AI Picture within the Menu gamestate and the point used for building the box for handling clicks. 
aiTogglePos :: World -> (Point, Point)
aiTogglePos w = ((pos1, (fst $ pos1 + 150, snd $ pos1 + 30)))
                        where pos1 = (fromIntegral (fst (windowSize w)) / (-2) + 1080, fromIntegral (snd (windowSize w)) / (-2) + 100)

-- | Returns the point used for translating the Reversi Mode Picture within the Menu gamestate and the point used for building the box for handling clicks. 
reversiTogglePos :: World -> (Point, Point)
reversiTogglePos w = ((pos1, (fst $ pos1 + 270, snd $ pos1 + 30)))
                        where pos1 = (fromIntegral (fst (windowSize w)) / (-2) + 80, fromIntegral (snd (windowSize w)) / (-2) + 50)

-- | Returns the point used for translating the Difficulty Picture within the Menu gamestate and the point used for building the box for handling clicks. 
aiDifficultyTogglePos :: World -> (Point, Point)
aiDifficultyTogglePos w = ((pos1, (fst $ pos1 + 270, snd $ pos1 + 30)))
	                        where pos1 = (fromIntegral (fst (windowSize w)) / (-2) + 1050, fromIntegral (snd (windowSize w)) / (-2) + 50)

-- | Returns the point used for translating the Pause Picture within the Playing gamestate and the point used for building the box for handling clicks.
pauseButtonPos :: World -> (Point, Point)
pauseButtonPos w = ((pos1, (fst $ pos1 + 100, snd $ pos1 + 30)))
	                        where pos1 = (fromIntegral (fst (windowSize w)) / (-2) + 1150, fromIntegral (snd (windowSize w)) / 2 - 50)

-- | Defines the offset by which the board is drawn.
boardDrawOffset :: (Float, Float)
boardDrawOffset = (0, 0)
