module Board where

import Data.List
import Debug.Trace

data Col = Black | White | Shadow
    deriving (Show, Eq) 

-- | Switches the colours, i.e. the indicator for turns
other :: Col -> Col
other Black = White
other White = Black
other Shadow = Shadow -- used with hints

data GameState = MainMenu | Paused | Playing | Finished | FinishedTimeLimit
    deriving (Show, Eq)

data Difficulty = Easy | Medium | Hard
    deriving Show

type Position = (Int, Int)

data Direction = North 
				| NorthEast
				| East
				| SouthEast
				| South
				| SouthWest
				| West 
				| NorthWest
	deriving Show

-- | Adjusts the Position given such that the returning Int tuple represents a movement in the specified Direction. 
moveDirection :: Direction -> Position -> (Int,Int) -- Its like a position, but not actually a position so we call it an (Int,Int)
moveDirection North (x,y)		= (x, y-1)
moveDirection NorthEast (x,y) 	= (x+1, y-1)
moveDirection East (x,y) 		= (x+1, y)
moveDirection SouthEast (x,y) 	= (x+1, y+1)
moveDirection South (x,y) 		= (x, y+1)
moveDirection SouthWest (x,y) 	= (x-1, y+1)
moveDirection West (x,y) 		= (x-1, y)
moveDirection NorthWest (x,y) 	= (x-1, y-1)

-- | Used by 'getDirection' (which calculates the difference between two points' x & y coordinates and places that difference in an int tuple) to determine the Direction of movement.
directionFromPosDiff :: (Int,Int) -> Direction
directionFromPosDiff (0,-1)		= North
directionFromPosDiff (1,-1) 	= NorthEast
directionFromPosDiff (1,0) 		= East
directionFromPosDiff (1,1) 		= SouthEast
directionFromPosDiff (0,1) 		= South
directionFromPosDiff (-1,1) 	= SouthWest
directionFromPosDiff (-1,0) 	= West
directionFromPosDiff (-1,-1) 	= NorthWest

-- | Gives the opposite Direction.
flipDirection :: Direction -> Direction
flipDirection North = South
flipDirection South = North
flipDirection West = East
flipDirection East = West
flipDirection NorthEast = SouthWest
flipDirection SouthWest = NorthEast
flipDirection SouthEast = NorthWest
flipDirection NorthWest = SouthEast

-- | A Board is a record containing the board size (a board is a square grid, n *
-- n), the number of consecutive passes, a list of pairs of position and
-- the colour at that position, boolean for if in reversiMode, and the size of the board tiles in pixels. 
data Board = Board { 
                     size :: Int,                   -- ^ The size of the board in squares. Default is 8. 4 and 16 are available.
                     passes :: Int,                 -- ^ The number of consecutive passes, incremented when a player can't make a move.
                     pieces :: [(Position, Col)],   -- ^ The list of pieces on this board, made up of a position and color.
                     reversiMode :: Bool,           -- ^ If true then the initial board has no pieces on it and players must place the first two in the centre of the board.
                     tileSizePixels :: Float,       -- ^ The size of the pieces in pixels (either 60 or 40 depending on board size).
                     aiCornersTaken :: Int,         -- ^ The number of corners the AI color has on this board.
                     aiAdjacentCornersTaken :: Int, -- ^ The number of squares adjacent to corners the AI has on this board.
                     playerCornersTaken :: Int      -- ^ The number of corners the human player has on this board.
                    }
  deriving (Show, Eq)

-- | Sets the initial board to 8x8 with neither player having passed and 4 initial pieces. 
initBoard :: Int -> Bool -> Float -> Board
initBoard size reversiMode tileSizePixels = if reversiMode then
                                                Board size 0 [] True tileSizePixels 0 0 0
                                            else 
                                                case size of 
                                                    8 ->  Board size 0 [((3,3), White), ((3,4), Black), ((4,3), Black), ((4,4), White)]  False tileSizePixels 0 0 0
                                                    4 ->  Board size 0 [((1,1), White), ((2,1), Black), ((1,2), Black), ((2,2), White)]  False tileSizePixels 0 0 0
                                                    16 -> Board size 0 [((7,7), White), ((8,7), Black), ((7,8), Black), ((8,8), White)]  False tileSizePixels 0 0 0

-- Overall state is the board and whose turn it is, plus any further
-- information about the world (this may later include, for example, player
-- names, timers, information about rule variants, etc)
--
-- Feel free to extend this, and 'Board' above with anything you think
-- will be useful (information for the AI, for example, such as where the
-- most recent moves were).

-- | Stores the current world state, has the board, whose turn it is, and all the game option flags.
data World = World { board          :: Board,       -- ^ The current game board
                     turn           :: Col,         -- ^ The current player
                     playerColor    :: Col,         -- ^ The color of the human player, or Player 1 in a human vs human game.
                     showHints      :: Bool,        -- ^ If true, display gray tiles on the board showing the available valid moves.
                     timePassed     :: Float,       -- ^ Is incremented in updateWorld until an event resets it such as making a move.
                     timeLimit      :: Float,       -- ^ The amount of time a player has to make their move.
                     winner         :: Col,         -- ^ The winner of the game, either has the most pieces at the end, or the other player's time ran out.
                     gameState      :: GameState,   -- ^ The current game state (Paused, Playing, MainMenu etc).
                     windowSize     :: (Int, Int),  -- ^ The size of the game window, always the same for us but the UI could react to different starting window sizes.
                     aiPlayer       :: Bool,        -- ^ If true then the AI plays with the color that isn't the player color.
                     prevBoards     :: [Board],     -- ^ A list of previous boards so that the player can undo moves and rewind the world state.
                     difficulty     :: Difficulty   -- ^ The difficulty level of the AI player.
                   }


-- | Initialises the World and conversely calls initBoard, and sets up the other factors of game play.
initWorld :: Int -> Col -> Bool -> Float -> (Int, Int) -> Float -> GameState -> Difficulty -> World
initWorld size playerColor reversiMode timeLimit windowSize tileSizePixels state difficulty = World (initBoard size reversiMode tileSizePixels) Black playerColor False 0 timeLimit Shadow state windowSize True [(initBoard size reversiMode tileSizePixels)] difficulty

-- | If the move is valid, makes the move, flipping the necessary pieces (flipPieces) and returning a new board with the new pieces.
makeMove :: Board -> Col -> Position -> Maybe Board
makeMove b col pos = case (validMove b col pos) of
							True -> Just b { pieces = newPieces }
							False -> Nothing
                     where newPieces = (pos, col):((pieces b) \\ [(x, other col) | x <- getPathPositions b col pos]) ++ flipPieces b col pos

isCornerMove :: Board -> Position -> Bool
isCornerMove b pos = pos `elem` [(0,0), (0,(size b)-1), ((size b)-1, 0), ((size b)-1, (size b)-1)]

-- | Calls makeMove and returns the World with the updated Board.
updateWorldWithBoard :: World -> Position -> World
updateWorldWithBoard w (x,y) = case makeMove (board w) (turn w) (x,y) of 
                                    Just newBoard -> addBoardToPrevious( w { board = newBoard { passes = 0, 
                                                                            aiCornersTaken =   if isCornerMove newBoard (x,y) && (turn w) == other (playerColor w) then 
                                                                                                        (aiCornersTaken newBoard) + 1
                                                                                                    else 
                                                                                                        (aiCornersTaken newBoard) 
                                                                            , playerCornersTaken = if isCornerMove newBoard (x,y) && (turn w) == playerColor w then 
                                                                                                        (playerCornersTaken newBoard) + 1
                                                                                                    else 
                                                                                                        (playerCornersTaken newBoard)
                                                                            , aiAdjacentCornersTaken = if isAdjacentCornerMove newBoard (x,y) && (turn w) == other (playerColor w) then
                                                                                                            (aiAdjacentCornersTaken newBoard) + 1 
                                                                                                        else
                                                                                                            aiAdjacentCornersTaken newBoard }
                                                                            , turn = other (turn w), timePassed = 0 } )
                                    Nothing -> w { showHints = not (showHints w) }

-- | Validates that the position chosen is ,in fact, within the list of valid moves. 
validMove :: Board -> Col -> Position -> Bool
validMove b col pos = pos `elem` getValidMoves b col

-- | Returns the list of valid move Positions. 
--   The implemented move validation works by filtering out moves that are not valid from a list of all moves on the board.
getValidMoves :: Board -> Col -> [Position] 
getValidMoves b col =   if (reversiMode b) && length (pieces b) < 4 then
                            filterEmpties b $ map fst $ pieces $ initBoard (size b) False (tileSizePixels b)
                        else 
                            filterOpenPaths b col $ filterEmpties b $ getAllCoordinates (size b)

-- | Gets all the positions adjacent to the corners on the given board.
getAdjacentCornerMoves :: Board -> [Position]
getAdjacentCornerMoves b =  [   (1,0), (0,1), (1,1), 
                                ((size b)-2,0), ((size b)-2,1), ((size b)-1, 1), 
                                (0,(size b)-2), (1,(size b)-1), (1,(size b)-2), 
                                ((size b)-2,(size b)-2), ((size b)-2, (size b)-1), ((size b)-1,(size b)-2) 
                            ]

-- | Returns true if the given position is adjacent to a corner on the given board.
isAdjacentCornerMove :: Board -> Position -> Bool
isAdjacentCornerMove b pos = pos `elem` getAdjacentCornerMoves b

-- | Gets all of the positions on the board. This is the list that the invalid moves get filtered out of. 
getAllCoordinates :: Int -> [Position]
getAllCoordinates size = [(x,y) | x <- [0..size-1], y <- [0..size-1]] --generated a list of tuples of ints representing all positions on the board

-- | Removes the positions that currently have pieces on them from the valid move list. 
filterEmpties :: Board -> [Position] -> [Position]
filterEmpties b poses = poses \\ map fst (pieces b) -- '\\' subtracts (uses Data.List) the filled positions from the list of all positions

-- | Returns the piece at a given position.
getPieceFromPos :: Board -> Position -> (Position, Col)
getPieceFromPos b pos = head [ piece | piece <- pieces b, fst piece == pos ] 

-- | Takes out the moves that don't have a 'capping' piece or piece of the same colour on the other side of a series of other colour pieces by walking  through the various potential pathways and checking those paths for a capping piece. 
-- Zip positions and directions into a tuple to make it easier to filter/curry stuff. map fst gets just the positions back out after the filter.
filterOpenPaths :: Board -> Col -> [Position] -> [Position]
filterOpenPaths b col poses = map fst $ filter (checkPath b col) (zip poses (map (getPathStarts b col) poses)) 

-- | Returns a list of Directions by which their corresponding pathway is one of the opposite colour. 
getPathStarts :: Board -> Col -> Position -> [Direction]
getPathStarts b col pos = map (getDirection pos) (getAdjacentsOppositeColor b col pos)

-- | Checks that the path is valid by calling 'checkDirectionalPath' for a capping piece. 
checkPath :: Board -> Col -> (Position, [Direction]) -> Bool
checkPath b col (pos, dirs) = any (checkDirectionalPath b col pos) dirs

-- | Checks if a path is closed by col. Initial pos passed in should be an adjacent to a possible move containing the opposite of col. 
checkDirectionalPath :: Board -> Col -> Position -> Direction -> Bool
checkDirectionalPath b col pos dir = 	if (moveDirection dir pos, col) `elem` (pieces b) then -- Found a capping color.
											True
										else if not (moveDirection dir pos `elem` map fst (pieces b)) then -- Empty or off board.
											False
										else
											checkDirectionalPath b col (moveDirection dir pos) dir -- Check the next tile.

-- | Builds piece-by-piece and returns the list of Positions in that path. 
getPath :: Board -> Col -> Position -> Direction -> [Maybe Position]
getPath b col pos dir =     if (moveDirection dir pos, col) `elem` (pieces b) then
                                []
                            else if (moveDirection dir pos, other col) `elem` (pieces b) then
                                Just (moveDirection dir pos):getPath b col (moveDirection dir pos) dir
                            else [Nothing]

-- | Takes the positions gathered from 'getPathPositions' and flips them using 'flipPiecesAtPositions'.
flipPieces :: Board -> Col -> Position -> [(Position, Col)]
flipPieces b col pos = flipPiecesAtPositions b col (getPathPositions b col pos)

-- | First gets the list of Positions in each Path (i.e. pieces in a consecutive Direction) and then applies 'extractPositions'.
getPathPositions :: Board -> Col -> Position -> [Position]
getPathPositions b col pos = concat (map extractPositions ((map (getPath b col pos) (getPathStarts b col pos))))

-- | Serves as the helper function to 'getPathPositions' which mainly ensures that the list is not empty before calling the helper function .
extractPositions :: [Maybe Position] -> [Position]
extractPositions   []   =  []
extractPositions poses =  if Nothing `elem` poses then
                            []
                          else
                            extractPositionsHelper poses 

-- | Builds the list of Positions to be flipped. 
extractPositionsHelper :: [Maybe Position] -> [Position]
extractPositionsHelper    []  = []
extractPositionsHelper (x:xs) = case x of
                                Just x -> x:extractPositionsHelper xs

-- | Changes the colour of the piece at Position to Col, used for the map in 'flipPieceAtPositions'.
flipPieceAtPos :: Board -> Col -> Position -> (Position, Col)
flipPieceAtPos b col pos = (pos, col)

-- | Moves through the list of Positions to be flipped and flips them using 'flipPieceAtPos' funciton.
flipPiecesAtPositions :: Board -> Col -> [Position] -> [(Position, Col)]
flipPiecesAtPositions b col poses = map (flipPieceAtPos b col) poses

-- | Takes the resulting Position form 'tupleAdd' and determines the Direction by which the two positions make.
getDirection :: Position -> Position -> Direction
getDirection (x1,y1) pos2 = directionFromPosDiff $ tupleAdd (-x1,-y1) pos2

-- | Returns the adjacent positions containing the opposite colour.
getAdjacentsOppositeColor :: Board -> Col -> Position -> [Position]
getAdjacentsOppositeColor b col pos = map fst $ filter (\ x -> snd x == other col) $ map (getPieceFromPos b) (getAdjacentPieces b pos)

-- | Get a list of adjacent positions that have a piece in them.
getAdjacentPieces :: Board -> Position -> [Position]
getAdjacentPieces b pos = intersect (getAdjacents pos) (map fst (pieces b)) 

-- | Generate a list of all 8 adjacent positions.
getAdjacents :: Position -> [Position]
getAdjacents pos = map (tupleAdd pos) [ (x,y) | x <- [-1,0,1], y <- [-1,0,1], not (x == 0 && y == 0) ]

-- | Adds the x-coordinates and y-coordinates of two positions and returns the resulting position
tupleAdd :: Position -> Position -> Position
tupleAdd (a,b) (c,d) = (a+c, b+d)

-- | Checks the current score using 'frequency' and returns a pair of the number of black pieces and the number of white pieces
checkScore :: Board -> (Int, Int)
checkScore b = (frequency Black $ map snd (pieces b) , frequency White $ map snd (pieces b))

-- | Used by 'checkScore' to increment and return the number of pieces with the same colour as Col. 
frequency :: Col -> [Col] -> Int
frequency y [] = 0
frequency y (x:xs) = case y == x of 
						True -> 1 + frequency y (xs)
						False -> frequency y xs


-- | Returns true if the game is complete (that is, either the board is full or there have been two consecutive passes).
gameOver :: Board -> Bool
gameOver b = if length (pieces b) >= (size b * size b) then
                True
             else if (passes b) >= 2 then --means both players cannot move 
                True
             else
                False

-- | Determines the winner by calculating which colour has the most pieces on the board.
determineWinner :: Board -> Col
determineWinner b = let winner = (length $ filter (\ x -> x == Black) (map snd (pieces b))) - (length $ filter (\ x -> x == White) (map snd (pieces b))) in
                    if winner > 0 then
                        Black
                    else if winner == 0 then
                        Shadow --must return a Col, this ultimately allows for the determination of a tie
                    else
                        White

-- | Used for the 'undo move' feature, returns a world with the board from the play before. 
--   During AI play, the resulting board is two boards before to allow for continued game play while for human-vs-human, the return is just the board before.
getBoardFromPrevious :: World -> World 
getBoardFromPrevious w = if ((aiPlayer w) == True && length (prevBoards w) > 2) then
                               
                            aiRemoveBoardFromPrev ( w {board = head (tail (tail (prevBoards w)))} ) --gets the second item in prevBoards because first item is AI's play
                        else if ((aiPlayer w) == False && length (prevBoards w) > 1) then
                            
                            userRemoveBoardFromPrev (w {board = head (tail (prevBoards w)), turn = other (turn w)} ) --must switch turn too
                        else 
                            w

-- | Removes the board from prevBoards (in World) upon clicking 'undo move'. Human-to-human just returns a World containing the tail of original list.
userRemoveBoardFromPrev :: World -> World
userRemoveBoardFromPrev w = if( (prevBoards w) == []) then
                                w
                            else
                                w {prevBoards = tail (prevBoards w)}

-- | Removes the board from prevBoards (in World) upon clicking 'undo move'. AI returns a World containing the tail of the tail of original list.
aiRemoveBoardFromPrev :: World -> World
aiRemoveBoardFromPrev w = if( (prevBoards w) == []) then
                            w
                          else
                            w {prevBoards = tail (tail (prevBoards w))}

-- | Adds the current board to prevBoards (in World).
addBoardToPrevious :: World -> World
addBoardToPrevious w = w { prevBoards = (board w) :(prevBoards w) }

-- | Harder evaluation function which aims for corners, attempts to stop opponent taking corners, col refers to the ENEMY. 
-- An evaluation function for a minimax search. Given a board and a colour
-- return an integer indicating how good the board is for that colour.
harderEvaluate :: (Board, Col) -> Int
harderEvaluate (b, col) =
                    -- Checks for boardstates in which we took a "C" or "X" square (adjacent to corner).
                    if 4 - (length $ [((0,0), other col), ((0,(size b)-1), other col), (((size b)-1, 0), other col), (((size b)-1, (size b)-1), other col)] \\ (pieces b)) > aiCornersTaken b then 
                        400 -- Board state in which we took a corner, very good. We will capture corners before trying to stop enemy from taking them.
                    else if 4 - (length $ [((0,0), col), ((0,(size b)-1), col), (((size b)-1, 0), col), (((size b)-1, (size b)-1), col)] \\ (pieces b)) > playerCornersTaken b then
                        0 -- Board state in which enemy took a corner, very bad, but not as important as us definitely taking a corner.
                    else if 12 - (length $  [ ((1,0), other col), ((0,1), other col), ((1,1), other col), 
                                        (((size b)-2,0), other col),(((size b)-2,1), other col), (((size b)-1, 1),other col), 
                                        ((0,(size b)-2),other col), ((1,(size b)-1),other col), ((1,(size b)-2),other col), 
                                        (((size b)-2,(size b)-2),other col), (((size b)-2, (size b)-1),other col), (((size b)-1,(size b)-2),other col)
                                       ] \\ (pieces b)) > aiAdjacentCornersTaken b
                         then
                            100
                    else 200 - (length $ getValidMoves b col)

-- | Easier evaluation function scores boards based on number of col pieces. 
easyEvaluate :: (Board, Col) -> Int
easyEvaluate (b, col) = frequency (other col) $ map snd $ pieces b


