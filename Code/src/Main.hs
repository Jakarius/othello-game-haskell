module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import System.Environment
import System.Exit

import Board
import Draw
import Input
import AI

-- 'play' starts up a graphics window and sets up handlers for dealing
-- with inputs and updating the world state.
--
-- 'drawWorld' converts the world state into a gloss Picture
--
-- 'handleInput' is called whenever there is an input event, and if it is
-- a human player's turn should update the board with the move indicated by
-- the event
--
-- 'updateWorld' is called 10 times per second (that's the "10" parameter)
-- and, if it is an AI's turn, should update the board with an AI generated
-- move

main :: IO ()
main =  do  args <- getArgs;
            --loads the necessary bitmap images of the boards
            board4  <- (loadBMP "res/4board.bmp")
            board8  <- (loadBMP "res/board.bmp")
            board16 <- (loadBMP "res/16board40px.bmp")

            --establishes the windowSize, used throughout the program for displaying the graphics
            windowSize <- return (1280,720)

            boardSize <- if (length args) > 0 then
                            case (read (args!!0) :: Int) of
                                4  -> return 4
                                8  -> return 8
                                16 -> return 16
                                otherwise -> return 8
                         else
                            return  8
            tileSizePixels <- case boardSize of
                                    4  -> return 60
                                    8  -> return 60
                                    16 -> return 40 --makes tiles smaller such that the larger board fits within the established graphical boundaries

            --loads the necessary bitmap images of the pieces and the Othello logo
            blackTile60 <- loadBMP "res/black.bmp";
            blackTile40 <- loadBMP "res/black40px.bmp";
            whiteTile60 <- loadBMP "res/white.bmp";
            whiteTile40 <- loadBMP "res/white40px.bmp";
            logopic <- loadBMP "res/othello.bmp"

            playerColor <-  if (length args) > 1 then
                                if (args!!1) == "black" then
                                    return Black
                                else if (args!!1) == "white" then
                                    return White
                                else
                                    return Black
                            else
                                return Black

            reversiMode <-  if (length args) > 2 then
                                if (args!!2) == "true" then
                                    return True
                                else
                                    return False
                            else
                                return False

            timeLimit <- if (length args) > 3 then
                            return (read (args!!3) :: Float)
                         else
                            return 0.0

            difficulty <- if (length args) > 4 then
                            case (args!!4) of
                                "hard"      -> return Hard
                                "HARD"      -> return Hard
                                "Hard"      -> return Hard
                                "medium"    -> return Medium
                                "MEDIUM"    -> return Medium
                                "Medium"    -> return Medium
                                "easy"      -> return Easy
                                "EASY"      -> return Easy
                                "Easy"      -> return Easy
                                otherwise   -> return Easy
                          else
                            return Easy
            -- starts game play
            play (InWindow "Othello" windowSize (10, 10)) black 10
                (initWorld boardSize playerColor reversiMode timeLimit windowSize tileSizePixels MainMenu difficulty) -- in Board.hs
                (drawWorld [board4, board8, board16] ([blackTile60,blackTile40], [whiteTile60,whiteTile40]) logopic) -- in Draw.hs
                handleInput -- in Input.hs
                updateWorld -- in AI.hs

